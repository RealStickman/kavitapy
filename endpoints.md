# Kavita Endpoints

Kavita version: 0.5.4  

Legend:  
- Normal: Endpoint currently unsupported
- **Bold**: Endpoint is supported. (partially/fully supported)

## Account
- POST - /api/Account/reset-password
- POST - /api/Account/register
- POST - /api/Account/login
- POST - /api/Account/refresh-token
- GET - /api/Account/roles
- POST - /api/Account/reset-api-key
- POST - /api/Account/update
- GET - /api/Account/invite-url
- POST - /api/Account/invite
- POST - /api/Account/confirm-email
- POST - /api/Account/confirm-password-reset
- POST - /api/Account/forgot-password
- POST - /api/Account/confirm-migration-email
- POST - /api/Account/resend-confirmation-email
- POST - /api/Account/migrate-email

## Collection
- GET - /api/Collection
- GET - /api/Collection/search
- POST - /api/Collection/update
- POST - /api/Collection/update-for-series
- POST - /api/Collection/update-series

## Download
- GET - /api/Download/volume-size
- GET - /api/Download/chapter-size
- GET - /api/Download/series-size
- GET - /api/Download/volume
- GET - /api/Download/chapter
- GET - /api/Download/series
- POST - /api/Download/bookmarks

## Image
- GET - /api/Image/chapter-cover
- GET - /api/Image/volume-cover
- GET - /api/Image/series-cover
- GET - /api/Image/collection-cover
- GET - /api/Image/readinglist-cover
- GET - /api/Image/bookmark
- GET - /api/Image/cover-upload

## Reader
- GET - /api/Reader/pdf
- GET - /api/Reader/image
- GET - /api/Reader/bookmark-image
- **GET - /api/Reader/chapter-info**
- GET - /api/Reader/bookmark-info
- POST - /api/Reader/mark-read
- POST - /api/Reader/mark-unread
- POST - /api/Reader/mark-volume-unread
- POST - /api/Reader/mark-volume-read
- POST - /api/Reader/mark-multiple-read
- POST - /api/Reader/mark-multiple-unread
- **POST - /api/Reader/mark-multiple-series-read**
- **POST - /api/Reader/mark-multiple-series-unread**
- **GET - /api/Reader/get-progress**
- **POST - /api/Reader/progress**
- **GET - /api/Reader/continue-point**
- GET - /api/Reader/has-progress
- GET - /api/Reader/get-bookmarks
- GET - /api/Reader/get-all-bookmarks
- POST - /api/Reader/remove-bookmarks
- POST - /api/Reader/bulk-remove-bookmarks
- GET - /api/Reader/get-volume-bookmarks
- GET - /api/Reader/get-series-bookmarks
- POST - /api/Reader/bookmark
- POST - /api/Reader/unbookmark
- GET - /api/Reader/next-chapter
- GET - /api/Reader/prev-chapter
- GET - /api/Reader/time-left

## Tachiyomi
- GET - /api/Tachiyomi/latest-chapter
- POST - /api/Tachiyomi/mark-chapter-until-as-read

## Upload
- POST - /api/Upload/upload-by-url
- POST - /api/Upload/series
- POST - /api/Upload/collection
- POST - /api/Upload/reading-list
- POST - /api/Upload/chapter
- POST - /api/Upload/reset-chapter-lock

## WantToRead
- POST - /api/want-to-read
- POST - /api/want-to-read/add-series
- POST - /api/want-to-read/remove-series

## Admin
- GET - /api/Admin/exists

## Book
- GET - /api/Book/{chapterId}/book-info
- GET - /api/Book/{chapterId}/book-resources
- GET - /api/Book/{chapterId}/chapters
- GET - /api/Book/{chapterId}/book-page

## Health
- GET - /api/Health

## Library
- POST - /api/Library/create
- GET - /api/Library/list
- **GET - /api/Library**
- GET - /api/Library/jump-bar
- POST - /api/Library/grant-access
- **POST - /api/Library/scan**
- POST - /api/Library/refresh-metadata
- POST - /api/Library/analyze
- **GET - /api/Library/libraries**
- **POST - /api/Library/scan-folder**
- DELETE - /api/Library/delete
- POST - /api/Library/update
- **GET - /api/Library/search**
- GET - /api/Library/type

## Metadata
- GET - /api/Metadata/genres
- GET - /api/Metadata/people
- GET - /api/Metadata/tags
- GET - /api/Metadata/age-ratings
- GET - /api/Metadata/publication-status
- GET - /api/Metadata/languages
- GET - /api/Metadata/all-languages
- GET - /api/Metadata/chapter-summary

## OPDS
- POST - /api/Opds/{apiKey}
- GET - /api/Opds/{apiKey}
- GET - /api/Opds/{apiKey}/libraries
- GET - /api/Opds/{apiKey}/collections
- GET - /api/Opds/{apiKey}/collections/{collectionId}
- GET - /api/Opds/{apiKey}/reading-list
- GET - /api/Opds/{apiKey}/reading-list/{readingListId}
- GET - /api/Opds/{apiKey}/libraries/{libraryId}
- GET - /api/Opds/{apiKey}/recently-added
- GET - /api/Opds/{apiKey}/on-deck
- GET - /api/Opds/{apiKey}/series
- GET - /api/Opds/{apiKey}/search
- GET - /api/Opds/{apiKey}/series/{seriesId}
- GET - /api/Opds/{apiKey}/series/{seriesId}/volume/{volumeId}
- GET - /api/Opds/{apiKey}/series/{seriesId}/volume/{volumeId}/chapter/{chapterId}
- GET - /api/Opds/{apiKey}/series/{seriesId}/volume/{volumeId}/chapter/{chapterId}/download/{filename}
- GET - /api/Opds/{apiKey}/image
- GET - /api/Opds/{apiKey}/favicon

## Plugin
- **POST - /api/Plugin/authenticate** *(always called in init of api)*

## ReadingList
- GET - /api/ReadingList
- DELETE - /api/ReadingList
- POST - /api/ReadingList/lists
- GET - /api/ReadingList/lists-for-series
- GET - /api/ReadingList/items
- POST - /api/ReadingList/update-position
- POST - /api/ReadingList/delete-item
- POST - /api/ReadingList/remove-read
- POST - /api/ReadingList/create
- POST - /api/ReadingList/update
- POST - /api/ReadingList/update-by-series
- POST - /api/ReadingList/update-by-multiple
- POST - /api/ReadingList/update-by-multiple-series
- POST - /api/ReadingList/update-by-volume
- POST - /api/ReadingList/update-by-chapter
- GET - /api/ReadingList/next-chapter
- GET - /api/ReadingList/prev-chapter

## Recommended
- GET - /api/Recommended/quick-reads
- GET - /api/Recommended/quick-catchup-reads
- GET - /api/Recommended/highly-rated
- GET - /api/Recommended/more-in
- GET - /api/Recommended/rediscover

## Series
- **POST - /api/Series** *(Function arguments don't provide access to the full request body. It's possible to provide a dictionary of the full request body)*
- GET - /api/Series/{seriesId}
- DELETE - /api/Series/{seriesId}
- POST - /api/Series/delete-multiple
- **GET - /api/Series/volumes**
- **GET - /api/Series/volume**
- GET - /api/Series/chapter
- GET - /api/Series/chapter-metadata
- POST - /api/Series/update-rating
- POST - /api/Series/update
- POST - /api/Series/recently-added
- POST - /api/Series/recently-updated-series
- POST - /api/Series/all
- POST - /api/Series/on-deck
- POST - /api/Series/refresh-metadata
- POST - /api/Series/scan
- POST - /api/Series/analyze
- GET - /api/Series/metadata
- POST - /api/Series/metadata
- GET - /api/Series/series-by-collection
- POST - /api/Series/series-by-ids
- GET - /api/Series/age-rating
- GET - /api/Series/series-detail
- GET - /api/Series/series-for-mangafile
- GET - /api/Series/series-for-chapter
- GET - /api/Series/related
- GET - /api/Series/all-related
- POST - /api/Series/update-related

## Server
- POST - /api/Server/restart
- POST - /api/Server/clear-cache
- POST - /api/Server/backup-db
- GET - /api/Server/server-info
- POST - /api/Server/convert-bookmarks
- GET - /api/Server/logs
- GET - /api/Server/check-update
- GET - /api/Server/changelog
- GET - /api/Server/accessible
- GET - /api/Server/jobs

## Settings
- GET - /api/Settings/base-url
- GET - /api/Settings
- POST - /api/Settings
- POST - /api/Settings/reset
- POST - /api/Settings/reset-email-url
- POST - /api/Settings/test-email-url
- GET - /api/Settings/task-frequencies
- GET - /api/Settings/library-types
- GET - /api/Settings/log-levels
- GET - /api/Settings/opds-enabled

## Theme
- GET - /api/Theme
- POST - /api/Theme/scan
- POST - /api/Theme/update-default
- GET - /api/Theme/download-content

## Users
- DELETE - /api/Users/delete-user
- GET - /api/Users
- GET - /api/Users/pending
- GET - /api/Users/has-reading-progress
- GET - /api/Users/has-library-access
- POST - /api/Users/update-preferences
- GET - /api/Users/get-preferences
